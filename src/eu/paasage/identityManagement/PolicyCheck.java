package eu.paasage.identityManagement;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openliberty.openaz.azapi.AzService;
import org.openliberty.openaz.azapi.pep.PepRequest;
import org.openliberty.openaz.azapi.pep.PepRequestFactory;
import org.openliberty.openaz.azapi.pep.PepResponse;
import org.openliberty.openaz.pdp.provider.AzServiceFactory;
import org.openliberty.openaz.pdp.provider.SimpleConcreteSunXacmlService;
import org.openliberty.openaz.pep.PepRequestFactoryImpl;

import test.policies.OpenAzLineContext;
import test.policies.OpenAzParseException;
import test.policies.OpenAzPolicyReader;
import test.policies.OpenAzXacmlObject;
import test.policies.OpenAzTokens.LineType;

import com.sun.xacml.ParsingException;
import com.sun.xacml.PolicySet;
import com.sun.xacml.UnknownIdentifierException;

/**
 *This class is based on code from the openAz project
 *We check user attributes hardwired into the code against a policy file stored on the local server
 */
public class PolicyCheck {

	public final static String CONTAINER = "orcl-weblogic"; 
	public final static String DEFAULT_PROVIDER_NAME = "AZ_SERVICE";
	public final static String SAMPLE_SESSION_USER_NAME = "Joe User";
	public static String combineAlg(String[] decisions) {
		int decisionsLen = decisions.length;
		String answer = null;
		for (int ii = 0; ii< decisions.length; ii++) {
			System.out.println("Length of array in loop = " + decisionsLen);
		}
		System.out.println("Length of array out of loop = " + decisionsLen);
		return answer;
	}

	/**
	 * Main method
	 * @param args
	 * @throws Exception 
	 */
	public String  check(String policyFile, String subjectId, String resourceId, String actionId) throws Exception  {
		System.out.println("Start Policy Check");

		
		// check xacml issue
	    String[] policyFiles = null;
		String requestFile = null; // not used; artifact from openaz testing
		StringWriter sw = new StringWriter();
		PolicyCheck policyCheck = new PolicyCheck();

			policyFiles = new String[]{policyFile};
		
		System.out.println("two-a");
		File xacmlPolicyFile = policyCheck.createXacmlPolicy(policyFile);		
		System.out.println("two-b");
		policyFiles = new String[]{xacmlPolicyFile.getPath()};
		System.out.println("two-c");
		
		// Print above before logging starts as sanity in case there
		//  are logging problems
		try {	Thread.sleep(100);
			System.out.println("Wait to print above before logging starts\n");
		} catch (InterruptedException e) {}
	

		try {
			// Register an instance of SimpleConcreteSunXacmlService
			//  as the AzService provider:

			System.out.println("One");
		
			
			
			AzServiceFactory.registerProvider(

					DEFAULT_PROVIDER_NAME, 
					new SimpleConcreteSunXacmlService(
							requestFile,
							policyFiles));

		} catch (ParsingException pe) {

			pe.printStackTrace(new PrintWriter(sw));
		} catch (UnknownIdentifierException uie) {

			uie.printStackTrace(new PrintWriter(sw));
		}

	

		// run the String input test using the registered provider
		String ret = policyCheck.checkPolicy(subjectId, resourceId, actionId);
		return ret;

	}


	//public void checkPolicy(String subjectId, String resourceId, String actionId) throws Exception {
	public String checkPolicy(String subjectId, String resourceId, String actionId) throws Exception {


		// Create the az service. In the "real world" this will be done
		// by the container and invisible to the application. We will
		// probably have some std object that users can refer to as
		// the handle to the az service much like AccessControlContext
		// is used in std java env. i.e. the following will ultimately
		// be reducible to az.decide(str, str, str) for a typical appl
		String result = null;
		AzService az = AzServiceFactory.getAzService();

		if (!(az==null)){
			// The PepRequestFactory is used to create az requests by
			// passing it the parameters that are needed for az, in
			// this case, 3 strings:
			// init the factory w the container name and the az service
			PepRequestFactory pepReqFactory = 
					(PepRequestFactory)new PepRequestFactoryImpl(CONTAINER,az);


			// Create the actual request using the factory to
			// create the request and passing the strings to
			// specify the details of the request
			System.out.println("Creating pepReq - \n\t  w subject,action,resource: " +
					subjectId + ", " + actionId + ", " + resourceId);
			PepRequest pepReq = 
					pepReqFactory.newPepRequest(
							subjectId, actionId, resourceId); 

			// Issue the request and receive a PepResponse object
			// in return, which contains details of the results
			// of the request
			PepResponse pepRspCtx = pepReq.decide();

			// Print the result (allowed true or false) and
			// then log the obligations, if any were returned.
			System.out.println("\n\t" + subjectId + 
					" allowed() = " + pepRspCtx.allowed() + "\n");
result = "\n\t" + subjectId + 
" allowed() = " + pepRspCtx.allowed() + "\n";
return result;
		}
		else
			System.out.println("az == null");
		
		return result;		
	}
	

	public File createXacmlPolicy(String policyFile) {
		byte[] policyBytes = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FileOutputStream fos = null;
		File xacmlPolicyFile = null;
		
		OpenAzLineContext oAzLineContext =
			new OpenAzLineContext(0, LineType.INIT);
		// create an OpenAzPolicyReader
		OpenAzPolicyReader oAzPolicyReader =
			new OpenAzPolicyReader(policyFile);
		try {
			OpenAzXacmlObject oAzXacmlObject =
				oAzPolicyReader.readAzLine(oAzLineContext);
			Object parsedObject = 
				oAzXacmlObject.getObject();
			PolicySet ps = (PolicySet) parsedObject;
		   ps.encode(baos);
			policyBytes = baos.toByteArray();
			//ps.encode(fw);
			// for the output file, we need a name
			// take existing name and append ".xml"
			System.out.println("writing the file");
			xacmlPolicyFile = new File(policyFile+".xacml");
			fos = new FileOutputStream(xacmlPolicyFile);
			ps.encode(fos);
			
		} catch (OpenAzParseException oape) {
		System.out.println(
				"Exception reading policy pseudo file: " +
				oape.getMessage());
		} catch (FileNotFoundException fnf) {
			System.out.println(
				"FileNotFoundException: " + fnf.getMessage());
		}
		
		return xacmlPolicyFile;
	}

	
}

