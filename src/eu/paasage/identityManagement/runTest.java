package eu.paasage.identityManagement;
public class runTest {

	public static void main(String args[]) throws Exception{

		//if no input supplied we populate with values

		if (args.length < 3){
			System.out.println("one or more inputs missing, tests needs action, target, subject and resouce inputs .... hardwiring values");
			args = new String[4];
			args[0] = "Read";
			args[1] = "URI-MyData1";
			args[2] = "tom";
			args[3] = "URI-MyData1";
		}
		//using the resource we ....
		//Retrieve the policy from the editor DB
		//Create the XACML for the policy
		//During XACML creation the CDO is checked for organisation (hardwired to AGH) if high read only access overwritter to policy
		Policy pol = new Policy();
		String policyFile = pol.getPolicy("URI-MyData1");
		//using the input into the test (action, target, subject, resource) we test the new policy
		String check = pol.invokeCheck(args, policyFile); 
		//and here is the result
		System.out.println("test result = " + check );


	}

}
